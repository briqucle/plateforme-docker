<?php

namespace App\Tests\Controller;

use App\Repository\ArticleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    /*** @test   */
    public function shouldListArticles()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertGreaterThan(0, $crawler->filter('.article')->count());
    }

    /*** @test   */
    public function shouldAlertOnEmptyArticlesList()
    {
        $stub = $this->createMock(ArticleRepository::class);
        $stub->method('findAll')->willReturn(new ArrayCollection());
        $client = static::createClient();
        static::$container->set('app.repository.articlerepository', $stub);
        $crawler = $client->request('GET', '/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals(0, $crawler->filter('.article')->count());
        $this->assertEquals(1, $crawler->filter('.no-article')->count());
    }
}