<?php declare(strict_types=1);

namespace App\Entity;
class Article
{
    private $id;
    private $text;
    private $createdAt;

    public function setId($id): Article
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): Article
    {
        $this->text = $text;
        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): Article
    {
        $this->createdAt = $createdAt;
        return $this;
    }
}